# assignment
Exercise:  

Step 1: Create a form input component. which contains input as first name, last name, a label for full name(frst name + lastname) which is non-editable and is updated as soon as user enters something in first or last name. 


Step 2: A user profile page, containing user details, and once edit button is clicked on this page, the above form pops up with edit details. And when submit is clicked, the modal closes and information is updated in user profile page. Profile page to be parent component and form should be child component. Communication between the two component is the point to be looked into.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
